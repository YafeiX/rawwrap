##Introduction

*miRAWResultFilterer* provides a way to consolidate and filter results generated from a miRNA target prediction run using miRAW. For details about miRAW see [our lab website](pinga.no"miRAW")

miRAW generates two output files *targetPredictionOutput.csv* and *positiveTargetSites.csv*. *targetPredictionOutput.csv* provides a summary of the prediction run in terms of total numbers of positive and negative sites for each prediction, as well as how many sites were removed by filtering (if relevant). *positiveTargetSites.csv* provides details about each positive site prediction, including location, predicted binding energy and site type (i.e., canonical or non-canonical). 

A prediction for a specific miRNA against a comprehensive set of mRNAs will typically return hundreds of predicted targets and it necessary to somehow prioritize these predictions for experimental verification. 

To generate a comprehensive report for each filtered prediction, we should also include sequence of miRNA and mRNA. thus, we also need the input file (generated with a *unifiedFile.csv* extension if running *miRAWbatch.py* to generate shell scripts for running miRAW in batch mode.)
